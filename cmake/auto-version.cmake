set(AUTO_VERSION_MODULE_DIR ${CMAKE_CURRENT_LIST_DIR})

# Supported types: LIBRARY, EXECUTABLE.
function(attach_auto_version type target_name)
	string(TOUPPER ${target_name} target_name_cpp)
	string(REPLACE - _ target_name_cpp ${target_name_cpp})

	message("C++ target name: ${target_name_cpp}")

	set(version_dir ${PROJECT_BINARY_DIR}/version/${target_name})

	message("Version directory: ${version_dir}")

	set(version_header_file "${version_dir}/version.h")
	set(version_source_file "${version_dir}/version.cpp")
	set(version_rc_file_ru "${version_dir}/info-ru.rc")
	set(version_rc_file_en "${version_dir}/info-en.rc")
	set(version_rc_file "${version_dir}/version.rc")
	set(binary_icon "${PROJECT_SOURCE_DIR}/resources/app.ico")

	set(out_files
		${version_header_file}
		${version_source_file}
		${version_rc_file_ru}
		${version_rc_file_en}
		${version_rc_file}
		${binary_icon}
	)

	set(out_src_files ${version_source_file} ${version_rc_file})

	if(type STREQUAL "EXECUTABLE")
		set(icon_enabled 1)
	else()
		set(icon_enabled 0)
	endif()

	if(icon_enabled AND (NOT EXISTS ${binary_icon}))
		set(binary_icon "${AUTO_VERSION_MODULE_DIR}/app.ico")
	endif()

	if(icon_enabled)
		message("[Target ${target_name}] Icon file: ${binary_icon}")
	endif()

	set(version_target_name ${target_name}-version-target)

	message("[Target ${target_name}] Attaching version target '${version_target_name}' to '${target_name}'")
	message("[Target ${target_name}] Additional files: ${out_src_files}")	

	add_custom_target(${version_target_name} ALL
		COMMAND ${CMAKE_COMMAND}
			-Dtarget_name_cpp=${target_name_cpp}
			-DPROJECT_VERSION_MAJOR=${PROJECT_VERSION_MAJOR}
			-DPROJECT_VERSION_MINOR=${PROJECT_VERSION_MINOR}
			-DPROJECT_VERSION_PATCH=${PROJECT_VERSION_PATCH}
			-DPROJECT_VERSION_TWEAK=${PROJECT_VERSION_TWEAK}
			-Dversion_header_file=${version_header_file}
			-Dversion_source_file=${version_source_file}
			-Dversion_rc_file_ru=${version_rc_file_ru}
			-Dversion_rc_file_en=${version_rc_file_en}
			-Dversion_rc_file=${version_rc_file}
			-Dbinary_icon=${binary_icon}
			-Dicon_enabled=${icon_enabled}
			-DPROJECT_SOURCE_DIR=${PROJECT_SOURCE_DIR}
			-P "${AUTO_VERSION_MODULE_DIR}/update-version-files.cmake"

		BYPRODUCTS ${out_src_files}
	)

	add_dependencies(${target_name} ${version_target_name})
	target_sources(${target_name} PRIVATE ${out_src_files})
	target_include_directories(${target_name} PRIVATE ${version_dir})

endfunction()
