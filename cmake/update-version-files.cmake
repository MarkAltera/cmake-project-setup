find_package(Git REQUIRED)

execute_process(
	COMMAND ${GIT_EXECUTABLE} rev-parse --short HEAD
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	OUTPUT_VARIABLE project_version_src_ref
	ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE
)

message("Updating ${version_header_file}")
configure_file(${CMAKE_CURRENT_LIST_DIR}/version.h.in ${version_header_file} @ONLY)

message("Updating ${version_source_file}")
configure_file(${CMAKE_CURRENT_LIST_DIR}/version.cpp.in ${version_source_file} @ONLY)

message("Updating ${version_rc_file_ru}")
configure_file(${CMAKE_CURRENT_LIST_DIR}/info-ru.rc.in ${version_rc_file_ru} @ONLY)

message("Updating ${version_rc_file_en}")
configure_file(${CMAKE_CURRENT_LIST_DIR}/info-en.rc.in ${version_rc_file_en} @ONLY)

message("Updating ${version_rc_file}")
configure_file(${CMAKE_CURRENT_LIST_DIR}/version.rc.in ${version_rc_file} @ONLY)

message("done")