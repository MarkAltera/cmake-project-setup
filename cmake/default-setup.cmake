include(auto-version)

set(DEFAULT_SETUP_MODULE_DIR ${CMAKE_CURRENT_LIST_DIR})

function(init_library_default_setup target_name)
    set_target_properties(${target_name} PROPERTIES DEBUG_POSTFIX d)

    target_include_directories(${target_name} PUBLIC include)
    target_include_directories(${target_name} PRIVATE include/${target_name})

    attach_auto_version(LIBRARY ${target_name})

    find_package(Doxygen)
    set(DOXYGEN_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/docs/${target_name})
    doxygen_add_docs(${target_name}-docs)

    install(TARGETS ${target_name} RUNTIME DESTINATION bin)
    install(FILES $<TARGET_PDB_FILE:${target_name}> DESTINATION bin OPTIONAL)
endfunction()


function(init_executable_default_setup target_name)
    set_target_properties(${target_name} PROPERTIES DEBUG_POSTFIX d)

    attach_auto_version(EXECUTABLE ${target_name})
    
    install(TARGETS ${target_name} RUNTIME DESTINATION bin)
    install(FILES $<TARGET_PDB_FILE:${target_name}> DESTINATION bin OPTIONAL)
endfunction()


function(init_qml_plugin_default_setup target_name module_name)
    attach_auto_version(LIBRARY ${target_name})

    message("[Target ${target_name}] Generating qmldir file ${CMAKE_INSTALL_PREFIX}/bin/imports/${module_name}/qmldir")
    configure_file(${DEFAULT_SETUP_MODULE_DIR}/qmldir.in ${CMAKE_INSTALL_PREFIX}/bin/imports/${module_name}/qmldir @ONLY)
    
    install(TARGETS ${target_name} RUNTIME DESTINATION bin/imports/${module_name})
    install(FILES $<TARGET_PDB_FILE:${target_name}> DESTINATION bin/imports/${module_name} OPTIONAL)
endfunction()