#include "version.h"

#include <lib/header.h>

#include <iostream>

int main()
{
	std::cout << "Hello, world!\n" << APP_VERSION_SRC_REF << "\n";
	lib::function();
}
