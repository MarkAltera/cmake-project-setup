cmake_minimum_required(VERSION 3.16.2)

project(app VERSION 2.3.4.0)

add_executable(${PROJECT_NAME}
    main.cpp
)

target_link_libraries(${PROJECT_NAME} lib)

include(default-setup)
init_executable_default_setup(${PROJECT_NAME})
