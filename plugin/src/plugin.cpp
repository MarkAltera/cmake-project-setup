#include "plugin.h"
#include "version.h"

void QmlPlugin::registerTypes(const char *uri)
{
    qmlRegisterType(QUrl("qrc:/MyQmlType3.qml"), uri, 1, 0, "MyQmlType3");
}
