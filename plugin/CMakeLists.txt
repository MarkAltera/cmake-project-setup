cmake_minimum_required(VERSION 3.16.2)

project(plugin VERSION 1.2.3.0)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

find_package(Qt5 COMPONENTS Core Qml)
find_package(Qt5QuickCompiler)

qtquick_compiler_add_resources(out_compiled_resources resources.qrc)

add_library(${PROJECT_NAME} SHARED
    src/plugin.cpp
    src/plugin.h
    ${out_compiled_resources}
)

target_link_libraries(${PROJECT_NAME} Qt5::Core Qt5::Qml)

include(default-setup)
init_qml_plugin_default_setup(${PROJECT_NAME} UserModule)
