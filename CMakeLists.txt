cmake_minimum_required(VERSION 3.16.2)

set(CMAKE_VERBOSE_MAKEFILE ON)

project(project VERSION 1.0.0)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")

add_subdirectory(app)
add_subdirectory(lib)
add_subdirectory(plugin)
