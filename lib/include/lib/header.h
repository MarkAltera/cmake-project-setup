/**
 * @file
 * @brief File.
 */

#pragma once

#ifdef lib_EXPORTS
	#define LIB_API __declspec(dllexport)
#else
	#define LIB_API __declspec(dllimport)
#endif


/**
 * @brief namespace.
 */
namespace lib
{
	/**
	 * @brief function.
	 */
	LIB_API void function();
}
