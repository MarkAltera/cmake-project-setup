#include "header.h"
#include "version.h"

#include <iostream>

namespace lib
{
	namespace
	{
		/**
		 * @brief helper function.
		 */
		void helper_function()
		{ }
	}

	void function()
	{
		std::cout
			<< "Library version: "
			<< LIB_VERSION_MAJOR << "." << LIB_VERSION_MINOR
			<< " src ref " << LIB_VERSION_SRC_REF;

		helper_function();
	}
}
